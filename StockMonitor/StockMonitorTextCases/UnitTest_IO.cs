﻿using StockMonitor;
using Common;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.Text;
using System.Collections.Generic;

namespace StockMonitorTextCases
{
    [TestClass]
    public class UnitTest_IO
    {
        [TestMethod]
        public void loading()
        {

            // I've never I/Oed from anything other then a .txt file... so, here we go!

            StockPortfolio portfolio = new StockPortfolio();
            string fileName = "";
            if (fileName == "") fileName = "CompanyList.csv";
            Assert.IsTrue(File.Exists(fileName));
            if (File.Exists(fileName))
            {
                int time = 1;
                var reader = new StreamReader(File.OpenRead(fileName));
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(',');
                    if (time == 1) Assert.AreEqual(values[0], "PIH");
                    if (time == 2) Assert.AreEqual(values[0], "FLWS");
                    if (time == 3) Assert.AreEqual(values[0], "FCCY");
                    if (time == 4) Assert.AreEqual(values[0], "SRCE");
                    if (time == 5) Assert.AreEqual(values[0], "XXII");
                    if (!portfolio.ContainsKey(values[0]))
                    {
                        portfolio.Add(values[0], new Stock() { });
                        portfolio[values[0]].symbol = values[0];
                    }
                    time++;
                    if (time > 5) break;
                }
                reader.Close();
            }
            else
            {
                Console.WriteLine("File \"" + fileName + "\" not found in:\n" + Directory.GetCurrentDirectory());
            }
        }

        [TestMethod]
        public void saving()
        {
            StringBuilder csvContent = new StringBuilder();
            List<string> list = new List<string>();
            list.Add("PIH");
            list.Add("FLWS");
            list.Add("FCCY");
            list.Add("SRCE");
            list.Add("XXII");
            foreach (var item in list)
            {
                csvContent.AppendLine(item.ToString());
            }
            string path = @".\portfolilo.csv";
            File.WriteAllText(path, csvContent.ToString());


            // testing the save:
            int counter = 0;
            List<string> list2 = new List<string>();
            string fileName = "portfolilo.csv";
            Assert.IsTrue(File.Exists(fileName));
            if (File.Exists(fileName))
            {
                var reader = new StreamReader(File.OpenRead(fileName));
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(',');
                    list2.Add(values[0]);
                    counter++;
                }
            }
            Assert.AreEqual(counter, 5);
            for(int i = 0; i < list.Count; i++)
                Assert.AreEqual(list[i], list2[i]);
        }
    }
}
