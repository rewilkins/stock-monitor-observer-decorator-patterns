﻿using Common;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Windows.Forms;
using System.Data;
using System.Windows.Forms.DataVisualization.Charting;
using System.Drawing;

namespace StockMonitorTextCases
{
    [TestClass]
    public class UnitTest_Connection
    {
        [TestMethod]
        public void Connection()
        {
            StockPortfolio portfolio = new StockPortfolio();
            Communicator communicator = new Communicator();
            string connection = "52.26.16.45:12099";

            // load Portfolio
            List<string> list2 = new List<string>();
            string fileName = "portfolilo.csv";
            Assert.IsTrue(File.Exists(fileName));
            if (File.Exists(fileName))
            {
                var reader = new StreamReader(File.OpenRead(fileName));
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(',');
                    portfolio.Add(values[0], new Stock() { });
                    portfolio[values[0]].symbol = values[0];
                }
            }

            IPEndPoint simulatorEndPoint = EndPointParser.Parse(connection);
            communicator.Portfolio = portfolio;
            communicator.RemoteEndPoint = simulatorEndPoint;
            communicator.Start();

            //VERIFICATION AND TESTING WAS DONE BY WATCHING THE SERVER START UP AND STOP.

            Assert.AreEqual(portfolio["PIH"].symbol, "PIH");
            System.Threading.Thread.Sleep(10000);

            StockPortfolio emptyPortfolio = new StockPortfolio();
            communicator.Portfolio = emptyPortfolio;
            communicator.Start();
        }
    }
}
