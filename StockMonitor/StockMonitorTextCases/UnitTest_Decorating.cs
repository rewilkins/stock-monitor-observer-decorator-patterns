﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Windows.Forms;
using System.Drawing;

// Decorator testing site...
namespace StockMonitorTextCases
{
    [TestClass]
    public abstract class PanelClass
    {
        public Panel panel = new Panel();
        public abstract void loadPanel();
    }

    [TestClass]
    public class ProfilePanel : PanelClass
    {
        Label label = new Label();

        override
        public void loadPanel()
        {
            label.Text = "Name...";
            label.Location = new Point(5, 5);
            panel.Controls.Add(label);
        }
    }

    [TestClass]
    public abstract class Decorator : PanelClass
    {
        override
        public abstract void loadPanel();

    }

    [TestClass]
    public class Addon : Decorator
    {
        Label label = new Label();

        PanelClass panelClass;
        public Addon(PanelClass panelClass)
        {
            this.panelClass = panelClass;
        }

        override
        public void loadPanel()
        {
            panelClass.loadPanel();
            panel = panelClass.panel;

            label.Text = "Age...";
            label.Location = new Point(15, 5);
            panel.Controls.Add(label);
        }
    }

    [TestClass]
    public class UnitTest_Decorating
    {
        [TestMethod]
        public void decorating()
        {
            PanelClass panelClass = new ProfilePanel();
            panelClass = new Addon(panelClass);
            panelClass.loadPanel();

            Assert.AreEqual(panelClass.panel.Controls.Count, 2);
            Assert.AreEqual(panelClass.panel.Controls[0].Text, "Name...");
            Assert.AreEqual(panelClass.panel.Controls[1].Text, "Age...");

        }
    }
}
