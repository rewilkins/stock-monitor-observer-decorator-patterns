﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class Stock
    {
        // TODO: Design and implement the properties and behaviors of this class.
        public int askPrice;
        public int averageVolume;
        public int bidPrice;
        public int currentPrice;
        public int currentVolume;
        public DateTime timestamp;
        public int openingPrice;
        public int previousClosingPrice;
        public string symbol;

        public void Update(TickerMessage message)
        {
            // TODO: Update the state of the stock object

            // For the moment, we'll have it simply write the new stock info to the console.  This code needs to be remove.
            // The observers will be responsible to presenting stock information to the user
            
            askPrice = message.AskPrice;
            averageVolume = message.AverageVolume;
            bidPrice = message.BidPrice;
            currentPrice = message.CurrentPrice;
            currentVolume = message.CurrentVolume;
            timestamp = (DateTime) message.MessageTimestamp;
            openingPrice = message.OpeningPrice;
            previousClosingPrice = message.PreviousClosingPrice;
            symbol = message.Symbol;

            //Console.WriteLine($"{message.Symbol}\tprice={message.CurrentPrice}\tvolumn={message.CurrentVolume}");
        }
    }
}
