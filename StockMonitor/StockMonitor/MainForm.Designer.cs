﻿namespace StockMonitor
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label_Colon = new System.Windows.Forms.Label();
            this.textBox_Port = new System.Windows.Forms.TextBox();
            this.textBox_IP = new System.Windows.Forms.TextBox();
            this.label_Connection = new System.Windows.Forms.Label();
            this.listBox_Portfolio = new System.Windows.Forms.ListBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuItem_Portfolio = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem_Modify = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem_Import = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem_Export = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem_Simulator = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem_Start = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem_Stop = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem_Connection = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem_Monitor = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem_PortfolioMonitor = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem_portOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem_portClose = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem_SelectedGraph = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem_priceOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem_priceClose = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem_SelectedVolume = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem_volumeOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem_volumeClose = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTextBox2 = new System.Windows.Forms.ToolStripTextBox();
            this.comboBox_Remove = new System.Windows.Forms.ToolStripComboBox();
            this.label_SimStatus = new System.Windows.Forms.Label();
            this.PanelDisplayArea = new System.Windows.Forms.Panel();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label_Colon
            // 
            this.label_Colon.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label_Colon.AutoSize = true;
            this.label_Colon.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Colon.Location = new System.Drawing.Point(661, 4);
            this.label_Colon.Name = "label_Colon";
            this.label_Colon.Size = new System.Drawing.Size(13, 17);
            this.label_Colon.TabIndex = 0;
            this.label_Colon.Text = ":";
            // 
            // textBox_Port
            // 
            this.textBox_Port.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_Port.Enabled = false;
            this.textBox_Port.Location = new System.Drawing.Point(680, 4);
            this.textBox_Port.Name = "textBox_Port";
            this.textBox_Port.Size = new System.Drawing.Size(100, 20);
            this.textBox_Port.TabIndex = 1;
            this.textBox_Port.Text = "12099";
            // 
            // textBox_IP
            // 
            this.textBox_IP.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_IP.Enabled = false;
            this.textBox_IP.Location = new System.Drawing.Point(555, 3);
            this.textBox_IP.Name = "textBox_IP";
            this.textBox_IP.Size = new System.Drawing.Size(100, 20);
            this.textBox_IP.TabIndex = 2;
            this.textBox_IP.Text = "52.26.16.45";
            // 
            // label_Connection
            // 
            this.label_Connection.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label_Connection.AutoSize = true;
            this.label_Connection.Location = new System.Drawing.Point(485, 6);
            this.label_Connection.Name = "label_Connection";
            this.label_Connection.Size = new System.Drawing.Size(64, 13);
            this.label_Connection.TabIndex = 3;
            this.label_Connection.Text = "Connection:";
            // 
            // listBox_Portfolio
            // 
            this.listBox_Portfolio.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.listBox_Portfolio.FormattingEnabled = true;
            this.listBox_Portfolio.Location = new System.Drawing.Point(13, 52);
            this.listBox_Portfolio.Name = "listBox_Portfolio";
            this.listBox_Portfolio.Size = new System.Drawing.Size(109, 446);
            this.listBox_Portfolio.Sorted = true;
            this.listBox_Portfolio.TabIndex = 6;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItem_Portfolio,
            this.menuItem_Simulator,
            this.menuItem_Monitor,
            this.toolStripTextBox2,
            this.comboBox_Remove});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(792, 27);
            this.menuStrip1.TabIndex = 7;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menuItem_Portfolio
            // 
            this.menuItem_Portfolio.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItem_Modify,
            this.menuItem_Import,
            this.menuItem_Export});
            this.menuItem_Portfolio.Name = "menuItem_Portfolio";
            this.menuItem_Portfolio.Size = new System.Drawing.Size(65, 23);
            this.menuItem_Portfolio.Text = "Portfolio";
            // 
            // menuItem_Modify
            // 
            this.menuItem_Modify.Name = "menuItem_Modify";
            this.menuItem_Modify.Size = new System.Drawing.Size(152, 22);
            this.menuItem_Modify.Text = "Modify";
            this.menuItem_Modify.Click += new System.EventHandler(this.menuItem_Modify_Click);
            // 
            // menuItem_Import
            // 
            this.menuItem_Import.Name = "menuItem_Import";
            this.menuItem_Import.Size = new System.Drawing.Size(152, 22);
            this.menuItem_Import.Text = "Import";
            this.menuItem_Import.Click += new System.EventHandler(this.menuItem_Import_Click);
            // 
            // menuItem_Export
            // 
            this.menuItem_Export.Name = "menuItem_Export";
            this.menuItem_Export.Size = new System.Drawing.Size(152, 22);
            this.menuItem_Export.Text = "Export";
            this.menuItem_Export.Click += new System.EventHandler(this.menuItem_Export_Click);
            // 
            // menuItem_Simulator
            // 
            this.menuItem_Simulator.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItem_Start,
            this.menuItem_Stop,
            this.menuItem_Connection});
            this.menuItem_Simulator.Name = "menuItem_Simulator";
            this.menuItem_Simulator.Size = new System.Drawing.Size(70, 23);
            this.menuItem_Simulator.Text = "Simulator";
            // 
            // menuItem_Start
            // 
            this.menuItem_Start.Name = "menuItem_Start";
            this.menuItem_Start.Size = new System.Drawing.Size(152, 22);
            this.menuItem_Start.Text = "Start";
            this.menuItem_Start.Click += new System.EventHandler(this.menuItem_Start_Click);
            // 
            // menuItem_Stop
            // 
            this.menuItem_Stop.Name = "menuItem_Stop";
            this.menuItem_Stop.Size = new System.Drawing.Size(152, 22);
            this.menuItem_Stop.Text = "Stop";
            this.menuItem_Stop.Click += new System.EventHandler(this.menuItem_Stop_Click);
            // 
            // menuItem_Connection
            // 
            this.menuItem_Connection.Name = "menuItem_Connection";
            this.menuItem_Connection.Size = new System.Drawing.Size(152, 22);
            this.menuItem_Connection.Text = "Connection";
            this.menuItem_Connection.Click += new System.EventHandler(this.menuItem_Connection_Click);
            // 
            // menuItem_Monitor
            // 
            this.menuItem_Monitor.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItem_PortfolioMonitor,
            this.menuItem_SelectedGraph,
            this.menuItem_SelectedVolume});
            this.menuItem_Monitor.Name = "menuItem_Monitor";
            this.menuItem_Monitor.Size = new System.Drawing.Size(62, 23);
            this.menuItem_Monitor.Text = "Monitor";
            // 
            // menuItem_PortfolioMonitor
            // 
            this.menuItem_PortfolioMonitor.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItem_portOpen,
            this.menuItem_portClose});
            this.menuItem_PortfolioMonitor.Name = "menuItem_PortfolioMonitor";
            this.menuItem_PortfolioMonitor.Size = new System.Drawing.Size(194, 22);
            this.menuItem_PortfolioMonitor.Text = "Portfolio";
            this.menuItem_PortfolioMonitor.Click += new System.EventHandler(this.menuItem_PortfolioMonitor_Click);
            // 
            // menuItem_portOpen
            // 
            this.menuItem_portOpen.Name = "menuItem_portOpen";
            this.menuItem_portOpen.Size = new System.Drawing.Size(160, 22);
            this.menuItem_portOpen.Text = "WIth Open Price";
            this.menuItem_portOpen.Click += new System.EventHandler(this.menuItem_portOpen_Click);
            // 
            // menuItem_portClose
            // 
            this.menuItem_portClose.Name = "menuItem_portClose";
            this.menuItem_portClose.Size = new System.Drawing.Size(160, 22);
            this.menuItem_portClose.Text = "WIth Close Price";
            this.menuItem_portClose.Click += new System.EventHandler(this.menuItem_portClose_Click);
            // 
            // menuItem_SelectedGraph
            // 
            this.menuItem_SelectedGraph.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItem_priceOpen,
            this.menuItem_priceClose});
            this.menuItem_SelectedGraph.Name = "menuItem_SelectedGraph";
            this.menuItem_SelectedGraph.Size = new System.Drawing.Size(194, 22);
            this.menuItem_SelectedGraph.Text = "Selected Stock Price";
            // 
            // menuItem_priceOpen
            // 
            this.menuItem_priceOpen.Name = "menuItem_priceOpen";
            this.menuItem_priceOpen.Size = new System.Drawing.Size(160, 22);
            this.menuItem_priceOpen.Text = "WIth Open Price";
            this.menuItem_priceOpen.Click += new System.EventHandler(this.menuItem_priceOpen_Click);
            // 
            // menuItem_priceClose
            // 
            this.menuItem_priceClose.Name = "menuItem_priceClose";
            this.menuItem_priceClose.Size = new System.Drawing.Size(160, 22);
            this.menuItem_priceClose.Text = "WIth Close Price";
            this.menuItem_priceClose.Click += new System.EventHandler(this.menuItem_priceClose_Click);
            // 
            // menuItem_SelectedVolume
            // 
            this.menuItem_SelectedVolume.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItem_volumeOpen,
            this.menuItem_volumeClose});
            this.menuItem_SelectedVolume.Name = "menuItem_SelectedVolume";
            this.menuItem_SelectedVolume.Size = new System.Drawing.Size(194, 22);
            this.menuItem_SelectedVolume.Text = "Selected Stock Volume";
            // 
            // menuItem_volumeOpen
            // 
            this.menuItem_volumeOpen.Name = "menuItem_volumeOpen";
            this.menuItem_volumeOpen.Size = new System.Drawing.Size(160, 22);
            this.menuItem_volumeOpen.Text = "WIth Open Price";
            this.menuItem_volumeOpen.Click += new System.EventHandler(this.menuItem_volumeOpen_Click);
            // 
            // menuItem_volumeClose
            // 
            this.menuItem_volumeClose.Name = "menuItem_volumeClose";
            this.menuItem_volumeClose.Size = new System.Drawing.Size(160, 22);
            this.menuItem_volumeClose.Text = "WIth Close Price";
            this.menuItem_volumeClose.Click += new System.EventHandler(this.menuItem_volumeClose_Click);
            // 
            // toolStripTextBox2
            // 
            this.toolStripTextBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.toolStripTextBox2.Enabled = false;
            this.toolStripTextBox2.Name = "toolStripTextBox2";
            this.toolStripTextBox2.Size = new System.Drawing.Size(100, 23);
            this.toolStripTextBox2.Text = "Remove Panel:";
            this.toolStripTextBox2.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // comboBox_Remove
            // 
            this.comboBox_Remove.Name = "comboBox_Remove";
            this.comboBox_Remove.Size = new System.Drawing.Size(121, 23);
            this.comboBox_Remove.Sorted = true;
            this.comboBox_Remove.SelectedIndexChanged += new System.EventHandler(this.removePanel);
            // 
            // label_SimStatus
            // 
            this.label_SimStatus.AutoSize = true;
            this.label_SimStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_SimStatus.ForeColor = System.Drawing.Color.Red;
            this.label_SimStatus.Location = new System.Drawing.Point(35, 34);
            this.label_SimStatus.Name = "label_SimStatus";
            this.label_SimStatus.Size = new System.Drawing.Size(60, 15);
            this.label_SimStatus.TabIndex = 8;
            this.label_SimStatus.Text = "Stopped";
            // 
            // PanelDisplayArea
            // 
            this.PanelDisplayArea.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PanelDisplayArea.AutoScroll = true;
            this.PanelDisplayArea.BackColor = System.Drawing.Color.White;
            this.PanelDisplayArea.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.PanelDisplayArea.Location = new System.Drawing.Point(129, 52);
            this.PanelDisplayArea.Name = "PanelDisplayArea";
            this.PanelDisplayArea.Size = new System.Drawing.Size(651, 446);
            this.PanelDisplayArea.TabIndex = 9;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(792, 514);
            this.Controls.Add(this.label_SimStatus);
            this.Controls.Add(this.PanelDisplayArea);
            this.Controls.Add(this.listBox_Portfolio);
            this.Controls.Add(this.label_Connection);
            this.Controls.Add(this.textBox_IP);
            this.Controls.Add(this.textBox_Port);
            this.Controls.Add(this.label_Colon);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_Colon;
        private System.Windows.Forms.TextBox textBox_Port;
        private System.Windows.Forms.TextBox textBox_IP;
        private System.Windows.Forms.Label label_Connection;
        private System.Windows.Forms.ListBox listBox_Portfolio;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuItem_Portfolio;
        private System.Windows.Forms.ToolStripMenuItem menuItem_Modify;
        private System.Windows.Forms.ToolStripMenuItem menuItem_Import;
        private System.Windows.Forms.ToolStripMenuItem menuItem_Export;
        private System.Windows.Forms.ToolStripMenuItem menuItem_Simulator;
        private System.Windows.Forms.ToolStripMenuItem menuItem_Start;
        private System.Windows.Forms.ToolStripMenuItem menuItem_Stop;
        private System.Windows.Forms.ToolStripMenuItem menuItem_Connection;
        private System.Windows.Forms.Label label_SimStatus;
        private System.Windows.Forms.ToolStripMenuItem menuItem_Monitor;
        private System.Windows.Forms.ToolStripMenuItem menuItem_PortfolioMonitor;
        private System.Windows.Forms.ToolStripMenuItem menuItem_SelectedGraph;
        private System.Windows.Forms.ToolStripMenuItem menuItem_SelectedVolume;
        private System.Windows.Forms.Panel PanelDisplayArea;
        private System.Windows.Forms.ToolStripComboBox comboBox_Remove;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox2;
        private System.Windows.Forms.ToolStripMenuItem menuItem_portOpen;
        private System.Windows.Forms.ToolStripMenuItem menuItem_portClose;
        private System.Windows.Forms.ToolStripMenuItem menuItem_priceOpen;
        private System.Windows.Forms.ToolStripMenuItem menuItem_priceClose;
        private System.Windows.Forms.ToolStripMenuItem menuItem_volumeOpen;
        private System.Windows.Forms.ToolStripMenuItem menuItem_volumeClose;
    }
}