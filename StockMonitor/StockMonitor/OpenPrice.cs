﻿using Common;
using System.Drawing;
using System.Windows.Forms;

namespace StockMonitor
{

    class OpenPrice : PanelDecorator
    {

        public OpenPrice(PanelClass panelClass)
        {
            this.panelClass = panelClass;
        }

        override
        public void clear()
        {
            panelClass.clear();
        }

        override
        public void refresh(StockPortfolio portfolio, ListBox list)
        {
            panelClass.refresh(portfolio, list);
            panel = panelClass.panel;

            if (price.Count == portfolio.Count)
            {
                for (int i = 0; i < portfolio.Count; i++)
                {
                    bool done = false;
                    while (done == false)
                    {
                        if (price[i].InvokeRequired)
                        {
                            price[i].Invoke(new MethodInvoker(delegate
                            {
                                price[i].Text = portfolio[list.Items[i].ToString()].openingPrice.ToString();
                                done = true;
                            }));
                        }
                    }
                }
            }
            else
            {
                if (price[0].InvokeRequired)
                {
                    price[0].Invoke(new MethodInvoker(delegate
                    {
                        price[0].Text = portfolio[list.Items[list.SelectedIndex].ToString()].openingPrice.ToString();
                    }));
                }
                
            }
            
        }

        override
        public void update(StockPortfolio portfolio, ListBox list)
        {
            
            panelClass.update(portfolio, list);
            
            ID = panelClass.ID;
            name = panelClass.name;
            title = panelClass.title;
            panel = panelClass.panel;

            Label openPrice = new Label();
            openPrice.Text = "Open";
            openPrice.Font = new Font(openPrice.Font.FontFamily, 12, openPrice.Font.Style | FontStyle.Bold | FontStyle.Underline);
            if(name == "Portfolio") openPrice.Location = new Point(235, 35);
            else openPrice.Location = new Point(235, 0);
            openPrice.AutoSize = true;
            panel.Controls.Add(openPrice);

            if (name == "Portfolio")
            {
                for (int i = 0; i < portfolio.Count; i++)
                {
                    price.Add(new Label());
                    price[i].Text = portfolio[list.Items[i].ToString()].askPrice.ToString();
                    price[i].Location = new Point(235, (i * 25) + 75);
                    price[i].AutoSize = true;
                    panel.Controls.Add(price[i]);
                }
            }
            else
            {
                price.Add(new Label());
                price[0].Text = portfolio[list.Items[list.SelectedIndex].ToString()].askPrice.ToString();
                price[0].Location = new Point(235, 25);
                price[0].AutoSize = true;
                panel.Controls.Add(price[0]);
            }


        }

    }
}
