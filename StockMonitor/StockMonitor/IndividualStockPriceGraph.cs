﻿using Common;
using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace StockMonitor
{
    class IndividualStockPriceGraph : PanelClass
    {

        private DataTable DT = new DataTable();
        private Chart graph = new Chart();
        private ChartArea graphArea = new ChartArea();
        private int time = 0;
        int low = 9999999;
        int high = -100;

        override
        public void update(StockPortfolio portfolio, ListBox list)
        {
            panel.Controls.Clear();
            //title.Text = "Price";
            title.Font = new Font(title.Font.FontFamily, 16, title.Font.Style | FontStyle.Bold);
            title.AutoSize = true;

            panel.AutoScroll = true;
            panel.Location = new Point(0, 0);
            panel.Size = new Size(300, 300);

            panel.BackColor = SystemColors.Control;
            panel.BorderStyle = BorderStyle.FixedSingle;

            panel.Controls.Add(title);


            // add the graph
            DT.Columns.Clear();
            DT.Columns.Add("Time", typeof(int));
            DT.Columns.Add("Value", typeof(int));

            graph.DataSource = DT;
            graph.DataBind();

            graph.BackColor = SystemColors.Control;
            graph.Location = new Point(20, 35);
            graph.Size = new Size(250, 250);

            graph.Series.Clear();
            graph.Series.Add("Data");
            graph.Series["Data"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            graph.Series["Data"].XValueMember = "Time";
            graph.Series["Data"].YValueMembers = "Value";
            graph.Series["Data"].AxisLabel = "";

            graphArea.AxisX.MajorGrid.Enabled = false;
            graphArea.AxisY.MajorGrid.Enabled = false;
            graphArea.BackColor = Color.White;
            graph.ChartAreas.Add(graphArea);
            graph.ChartAreas[0].AxisX.Interval = 1;
            graph.ChartAreas[0].AxisX.LabelStyle.Enabled = false;
            graph.ChartAreas[0].AxisY.LabelStyle.Enabled = false;

            graph.Invalidate();
            panel.Controls.Add(graph);

        }

        override
        public void refresh(StockPortfolio portfolio, ListBox list)
        {

            low = 9999999;
            high = -100;
            foreach (DataRow dr in DT.Rows) {
                if (Int32.Parse(dr["Value"].ToString()) > high) high = Int32.Parse(dr["Value"].ToString());
                if (Int32.Parse(dr["Value"].ToString()) < low) low = Int32.Parse(dr["Value"].ToString());
            }

            bool done = false;
            while (done == false)
            {
                if (graph.InvokeRequired)
                {
                    graph.Invoke(new MethodInvoker(delegate
                    {
                        int currentPrice = portfolio[name].currentPrice;
                        DT.Rows.Add(new object[] { time, currentPrice });

                        while (DT.Rows.Count > 60)
                        {
                            DT.Rows.RemoveAt(0);
                        }
                        graph.DataBind(); // force chart to refresh

                        if (time < 31)
                        {
                            graph.ChartAreas[0].AxisX.Maximum = 30;
                            graph.ChartAreas[0].AxisX.Minimum = 0;
                        }
                        else
                        {
                            graph.ChartAreas[0].AxisX.Maximum = time;
                            graph.ChartAreas[0].AxisX.Minimum = time - 30;
                        }

                        if (low >= high)
                        {
                            graph.ChartAreas[0].AxisY.Maximum = 5;
                            graph.ChartAreas[0].AxisY.Minimum = 0;
                        }
                        else
                        {
                            graph.ChartAreas[0].AxisY.Maximum = high + 1;
                            graph.ChartAreas[0].AxisY.Minimum = low - 1;
                        }

                        done = true;

                    }));
                }
            }

            if (DT.Rows.Count > 1)
            {
                DT.Rows[0]["Value"] = DT.Rows[1]["Value"];
            }
            time++;

        }

        override
        public void clear()
        {
            DT.Clear();
            graph.DataBind();
        }

    }
}
