﻿using Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StockMonitor
{
    public partial class ModifyPortfolioForm : Form
    {

        public StockPortfolio returnPortfolio = new StockPortfolio();
        public StockPortfolio returnMarket = new StockPortfolio();

        public ModifyPortfolioForm(StockPortfolio market, StockPortfolio portfolio)
        {
            InitializeComponent();

            //returnMarket = market;
            //returnPortfolio = portfolio;
            foreach (var stock in market)
            {
                listBox_Market.Items.Add(stock.Value.symbol);
            }
            foreach (var stock in portfolio)
            {
                listBox_Portfolio.Items.Add(stock.Value.symbol);
            }

            //listBox_Portfolio.Items.Clear();
            //object[] portfolioCollection = new object[portfolio.Items.Count];
            //portfolio.Items.CopyTo(portfolioCollection, 0);
            //listBox_Portfolio.Items.AddRange(portfolioCollection);

        }

        private void button_Remove_Click(object sender, EventArgs e)
        {
            if (listBox_Portfolio.SelectedIndex > -1)
            {
                var item = listBox_Portfolio.SelectedItem;
                int selectedID = listBox_Portfolio.SelectedIndex;

                listBox_Portfolio.Items.Remove(item);
                listBox_Market.Items.Add(item);
                
                if (selectedID != listBox_Portfolio.Items.Count)
                {
                    listBox_Portfolio.SelectedIndex = selectedID;
                }
            }
        }

        private void button_Add_Click(object sender, EventArgs e)
        {
            if (listBox_Market.SelectedIndex > -1)
            {
                var item = listBox_Market.SelectedItem;
                int selectedID = listBox_Market.SelectedIndex;

                listBox_Market.Items.Remove(item);
                listBox_Portfolio.Items.Add(item);
                
                if (selectedID != listBox_Market.Items.Count)
                {
                    listBox_Market.SelectedIndex = selectedID;
                }
            }
        }

        private void button_Save_Click(object sender, EventArgs e)
        {
            foreach (var item in listBox_Market.Items)
            {
                returnMarket.Add(item.ToString(), new Stock() { });
                returnMarket[item.ToString()].symbol = item.ToString();
            }
            foreach (var item in listBox_Portfolio.Items)
            {
                returnPortfolio.Add(item.ToString(), new Stock() { });
                returnPortfolio[item.ToString()].symbol = item.ToString();
            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void button_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }
    }
}
