﻿using Common;
using System.Windows.Forms;

namespace StockMonitor
{
    abstract class PanelClass
    {
        public int ID = 0;
        public string name;
        public Label title = new Label();
        public Panel panel = new Panel();
        public abstract void update(StockPortfolio portfolio, ListBox list);
        public abstract void refresh(StockPortfolio portfolio, ListBox list);
        public abstract void clear();
    }
}
