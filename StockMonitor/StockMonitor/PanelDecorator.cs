﻿using System.Collections.Generic;
using System.Windows.Forms;
using Common;

namespace StockMonitor
{
    abstract class PanelDecorator : PanelClass
    {
        public List<Label> price = new List<Label>();
        public PanelClass panelClass;
        //public PanelClass panelPortfolio = new PortfolioStockPrices();

        public PanelClass getPanelClass()
        {
            return panelClass;
        }

        override
        public abstract void clear();

        override
        public abstract void refresh(StockPortfolio portfolio, ListBox list);

        override
        public abstract void update(StockPortfolio portfolio, ListBox list);
    }
}
