﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;

using Common;
using System.IO;
using System.Threading;

namespace StockMonitor
{
    public partial class MainForm : Form
    {

        StockPortfolio market = new StockPortfolio();
        StockPortfolio portfolio = new StockPortfolio();
        Communicator communicator = new Communicator();
        List<PanelClass> panels = new List<PanelClass>();

        public MainForm()
        {
            InitializeComponent();

            string fileName = "CompanyList.csv";
            if (File.Exists(fileName))
            {
                var reader = new StreamReader(File.OpenRead(fileName));
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(',');
                    if (!market.ContainsKey(values[0]))
                    {
                        market.Add(values[0], new Stock() { });
                        market[values[0]].symbol = values[0];
                    }
                }
            }
            else
            {
                MessageBox.Show("File \"" + fileName + "\" not found in:\n" + Directory.GetCurrentDirectory());
            }

        }

        private void updatePanels()
        {
            for(int i = 0; i < panels.Count(); i++)
            {
                if (i % 2 == 0 || panels.Count() == 1) // left column
                {
                    panels[i].panel.Location = new Point(PanelDisplayArea.AutoScrollPosition.X + 10, ((310 * (i / 2)) + PanelDisplayArea.AutoScrollPosition.Y) + 10);
                }
                else // right column
                {
                    panels[i].panel.Location = new Point(PanelDisplayArea.AutoScrollPosition.X + 320, ((310 * (i / 2)) + PanelDisplayArea.AutoScrollPosition.Y) + 10);
                }
            }

        }

        private void menuItem_Connection_Click(object sender, EventArgs e)
        {
            var form = new ConnectionForm();
            var result = form.ShowDialog();
            if (result == DialogResult.OK)
            {

                label_SimStatus.Text = "Stopped";
                label_SimStatus.ForeColor = Color.Red;
                communicator.Stop();

                string IP = form.ReturnIP;  //values preserved after close
                string Port = form.ReturnPort;  //values preserved after close

                textBox_IP.Text = IP;
                textBox_Port.Text = Port;

            }
        }

        private void menuItem_Start_Click(object sender, EventArgs e)
        {
            if(!string.IsNullOrEmpty(textBox_IP.Text) && !string.IsNullOrEmpty(textBox_Port.Text))
            {
                label_SimStatus.Text = "Running";
                label_SimStatus.ForeColor = Color.Green;

                string connection = "";
                connection += textBox_IP.Text;
                connection += ":";
                connection += textBox_Port.Text;

                IPEndPoint simulatorEndPoint = EndPointParser.Parse(connection);

                communicator.Portfolio = portfolio;
                communicator.RemoteEndPoint = simulatorEndPoint;
                communicator.Start();

                // update form on different thread
                Thread graphThread = new Thread(new ThreadStart(refreshGraphs));
                graphThread.Start();
                Thread portfolioThread = new Thread(new ThreadStart(refreshPortfolio));
                portfolioThread.Start();

            }
        }

        private void menuItem_Stop_Click(object sender, EventArgs e)
        {
            label_SimStatus.Text = "Stopped";
            label_SimStatus.ForeColor = Color.Red;
            communicator.Stop();
            for(int i = 0; i < panels.Count; i++)
            {
                panels[i].clear();
            }
        }

        void refreshGraphs()
        {
            while (label_SimStatus.Text == "Running")
            {

                for (int i = 0; i < panels.Count(); i++)
                {
                    if (panels[i].name != "Portfolio")
                    {
                        panels[i].refresh(portfolio, listBox_Portfolio);
                    }
                }
                System.Threading.Thread.Sleep(60000);
            }
        }

        void refreshPortfolio()
        {
            while (label_SimStatus.Text == "Running")
            {

                for (int i = 0; i < panels.Count(); i++)
                {
                    if (panels[i].name == "Portfolio")
                    {
                        panels[i].refresh(portfolio, listBox_Portfolio);
                    }
                }
                System.Threading.Thread.Sleep(1000);
            }
        }

        private void menuItem_Modify_Click(object sender, EventArgs e)
        {
            var form = new ModifyPortfolioForm(market, portfolio);
            var result = form.ShowDialog();

            if (result == DialogResult.OK)
            {
                // update portfolio list
                portfolio = form.returnPortfolio;
                market = form.returnMarket;

                listBox_Portfolio.Items.Clear();
                foreach (var stock in portfolio)
                {
                    listBox_Portfolio.Items.Add(stock.Value.symbol);
                }
            }
            // update portfolio
            if (label_SimStatus.Text == "Running")
            {
                communicator.Portfolio = portfolio;
                //communicator.Refresh();
            }

            // update panels
            //if (result == DialogResult.OK)
            //{
                int ID = 0;
                bool found = true;
                while (found)
                {
                    found = false;
                    ID++;
                    for (int i = 0; i < panels.Count(); i++)
                    {
                        if (panels[i].title.Text == "Portfolio " + ID)
                        {
                            found = true;
                            panels[i].update(portfolio, listBox_Portfolio);
                        }
                    }
                }

                found = true;
                bool removedOne = true;
                while (removedOne == true)
                {
                    removedOne = false;
                    for (int i = 0; i < panels.Count; i++)
                    {
                        found = false;
                        
                        for (int j = 0; j < listBox_Portfolio.Items.Count; j++)
                        {
                            if (panels[i].name == listBox_Portfolio.Items[j].ToString())
                            {
                                found = true;
                            }
                        }
                        if (!found && panels[i].name != "Portfolio")
                        {
                            removePanel2(panels[i].title.Text);
                            removedOne = true;  // removed one so we much go around the loop again...
                        }
                    }
                }
            //}
            updatePanels();

        }

        private void menuItem_Import_Click(object sender, EventArgs e)
        {

            while (comboBox_Remove.Items.Count > 0)
            {
                for (int i = 0; i < comboBox_Remove.Items.Count; i++)
                {
                    removePanel2(comboBox_Remove.Items[i].ToString());
                }
            }

            listBox_Portfolio.Items.Clear();
            portfolio.Clear();
            string fileName = "portfolilo.csv";
            if (File.Exists(fileName))
            {
                var reader = new StreamReader(File.OpenRead(fileName));
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(',');
                    if(!portfolio.ContainsKey(values[0]))
                    {
                        portfolio.Add(values[0], new Stock() { });
                        listBox_Portfolio.Items.Add(values[0]);
                        portfolio[values[0]].symbol = values[0];
                    }
                }
                reader.Close();
                
            }
            else
            {
                MessageBox.Show("File \"" + fileName + "\" not found in:\n" + Directory.GetCurrentDirectory());
            }

            foreach (var stock in portfolio) {
                market.Remove(stock.Value.symbol);
            }

            // send the updated list to the simulator if the communicator is monitoring
            if (label_SimStatus.Text == "Running")
            {
                communicator.Portfolio = portfolio;
                communicator.Refresh();
            }

        }

        private void menuItem_Export_Click(object sender, EventArgs e)
        {
            StringBuilder csvContent = new StringBuilder();
            foreach (var stock in listBox_Portfolio.Items)
            {
                csvContent.AppendLine(stock.ToString());
            }
            string path = @".\portfolilo.csv";
            File.WriteAllText(path, csvContent.ToString());
        }

        private void removePanel(object sender, EventArgs e)
        {
            removePanel2(sender.ToString());
        }

        private void removePanel2(string selectedName)
        {
            var Name = selectedName.Split(',');
            comboBox_Remove.Items.Remove(Name[0]);
            for (int i = panels.Count() - 1; i >= 0; i--)
            {
                if (panels[i].title.Text == Name[0])
                {
                    PanelDisplayArea.Controls.Remove(panels[i].panel);
                    panels.RemoveAt(i);
                }
            }
            updatePanels();
        }

        private void menuItem_portOpen_Click(object sender, EventArgs e)
        {
            PanelClass portfolioPanel = new PortfolioStockPrices();
            portfolioPanel.update(portfolio, listBox_Portfolio);

            int ID = 0;
            bool found = true;
            while (found)
            {
                found = false;
                ID++;
                for (int i = 0; i < panels.Count(); i++)
                {
                    if (panels[i].title.Text == "Portfolio " + ID)
                    {
                        found = true;
                    }
                }
            }
            portfolioPanel.ID = ID;
            portfolioPanel.name = portfolioPanel.title.Text;
            portfolioPanel.title.Text = portfolioPanel.title.Text + " " + ID;

            portfolioPanel = new OpenPrice(portfolioPanel);
            portfolioPanel.update(portfolio, listBox_Portfolio);

            PanelDisplayArea.Controls.Add(portfolioPanel.panel);
            panels.Add(portfolioPanel);

            comboBox_Remove.Items.Add(portfolioPanel.title.Text);

            updatePanels();
        }

        private void menuItem_portClose_Click(object sender, EventArgs e)
        {
            PanelClass portfolioPanel = new PortfolioStockPrices();
            portfolioPanel.update(portfolio, listBox_Portfolio);


            int ID = 0;
            bool found = true;
            while (found)
            {
                found = false;
                ID++;
                for (int i = 0; i < panels.Count(); i++)
                {
                    if (panels[i].title.Text == "Portfolio " + ID)
                    {
                        found = true;
                    }
                }
            }
            portfolioPanel.ID = ID;
            portfolioPanel.name = portfolioPanel.title.Text;
            portfolioPanel.title.Text = portfolioPanel.title.Text + " " + ID;

            portfolioPanel = new ClosePrice(portfolioPanel);
            portfolioPanel.update(portfolio, listBox_Portfolio);

            PanelDisplayArea.Controls.Add(portfolioPanel.panel);
            panels.Add(portfolioPanel);

            comboBox_Remove.Items.Add(portfolioPanel.title.Text);

            updatePanels();
        }

        private void menuItem_priceOpen_Click(object sender, EventArgs e)
        {
            if (listBox_Portfolio.SelectedIndex != -1)
            {
                PanelClass portfolioPanel = new IndividualStockPriceGraph();
                //portfolioPanel.update(portfolio, listBox_Portfolio);

                portfolioPanel.name = listBox_Portfolio.Items[listBox_Portfolio.SelectedIndex].ToString();
                portfolioPanel.title.Text = "Price" + " " + listBox_Portfolio.Items[listBox_Portfolio.SelectedIndex].ToString();

                bool found = false;
                foreach (var item in comboBox_Remove.Items)
                {
                    if (item.ToString() == ("Price " + listBox_Portfolio.Items[listBox_Portfolio.SelectedIndex].ToString()))
                    {
                        found = true;
                    }
                }

                if (!found)
                {
                    comboBox_Remove.Items.Add(portfolioPanel.title.Text);
                }

                portfolioPanel = new OpenPrice(portfolioPanel);
                portfolioPanel.update(portfolio, listBox_Portfolio);

                PanelDisplayArea.Controls.Add(portfolioPanel.panel);
                panels.Add(portfolioPanel);

                updatePanels();
            }
        }

        private void menuItem_priceClose_Click(object sender, EventArgs e)
        {
            if (listBox_Portfolio.SelectedIndex != -1)
            {
                PanelClass portfolioPanel = new IndividualStockPriceGraph();
                //portfolioPanel.update(portfolio, listBox_Portfolio);

                portfolioPanel.name = listBox_Portfolio.Items[listBox_Portfolio.SelectedIndex].ToString();
                portfolioPanel.title.Text = "Price" + " " + listBox_Portfolio.Items[listBox_Portfolio.SelectedIndex].ToString();

                bool found = false;
                foreach (var item in comboBox_Remove.Items)
                {
                    if (item.ToString() == ("Price " + listBox_Portfolio.Items[listBox_Portfolio.SelectedIndex].ToString()))
                    {
                        found = true;
                    }
                }

                if (!found)
                {
                    comboBox_Remove.Items.Add(portfolioPanel.title.Text);
                }

                portfolioPanel = new ClosePrice(portfolioPanel);
                portfolioPanel.update(portfolio, listBox_Portfolio);

                PanelDisplayArea.Controls.Add(portfolioPanel.panel);
                panels.Add(portfolioPanel);

                updatePanels();
            }
        }

        private void menuItem_volumeOpen_Click(object sender, EventArgs e)
        {
            if (listBox_Portfolio.SelectedIndex != -1)
            {
                PanelClass portfolioPanel = new IndividualStockVolumeGraph();
                //portfolioPanel.update(portfolio, listBox_Portfolio);

                portfolioPanel.name = listBox_Portfolio.Items[listBox_Portfolio.SelectedIndex].ToString();
                portfolioPanel.title.Text = "Volume" + " " + listBox_Portfolio.Items[listBox_Portfolio.SelectedIndex].ToString();

                bool found = false;
                foreach (var item in comboBox_Remove.Items)
                {
                    if (item.ToString() == ("Volume " + listBox_Portfolio.Items[listBox_Portfolio.SelectedIndex].ToString()))
                    {
                        found = true;
                    }
                }

                if (!found)
                {
                    comboBox_Remove.Items.Add(portfolioPanel.title.Text);
                }

                portfolioPanel = new OpenPrice(portfolioPanel);
                portfolioPanel.update(portfolio, listBox_Portfolio);

                PanelDisplayArea.Controls.Add(portfolioPanel.panel);
                panels.Add(portfolioPanel);

                updatePanels();
            }
        }

        private void menuItem_volumeClose_Click(object sender, EventArgs e)
        {
            if (listBox_Portfolio.SelectedIndex != -1)
            {
                PanelClass portfolioPanel = new IndividualStockVolumeGraph();
                //portfolioPanel.update(portfolio, listBox_Portfolio);

                portfolioPanel.name = listBox_Portfolio.Items[listBox_Portfolio.SelectedIndex].ToString();
                portfolioPanel.title.Text = "Volume" + " " + listBox_Portfolio.Items[listBox_Portfolio.SelectedIndex].ToString();

                bool found = false;
                foreach (var item in comboBox_Remove.Items)
                {
                    if (item.ToString() == ("Volume " + listBox_Portfolio.Items[listBox_Portfolio.SelectedIndex].ToString()))
                    {
                        found = true;
                    }
                }

                if (!found)
                {
                    comboBox_Remove.Items.Add(portfolioPanel.title.Text);
                }

                portfolioPanel = new ClosePrice(portfolioPanel);
                portfolioPanel.update(portfolio, listBox_Portfolio);

                PanelDisplayArea.Controls.Add(portfolioPanel.panel);
                panels.Add(portfolioPanel);

                updatePanels();
            }
        }

        private void menuItem_PortfolioMonitor_Click(object sender, EventArgs e)
        {

        }
    }
}
