﻿using Common;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Drawing;

namespace StockMonitor
{
    class PortfolioStockPrices : PanelClass
    {

        List<int> lastPrices = new List<int>();
        List<Label> currentPrices = new List<Label>();
        List<Label> directions = new List<Label>();
        List<Label> bids = new List<Label>();
        List<Label> askPrice = new List<Label>();

        override
        public void update(StockPortfolio portfolio, ListBox list)
        {
            panel.Controls.Clear();
            if (ID > 0) title.Text = "Portfolio " + ID;
            else title.Text = "Portfolio";
            title.Font = new Font(title.Font.FontFamily, 16, title.Font.Style | FontStyle.Bold);
            title.AutoSize = true;

            panel.AutoScroll = true;
            panel.Location = new Point(0, 0);
            panel.Size = new Size(300, 300);

            panel.BackColor = SystemColors.Control;
            panel.BorderStyle = BorderStyle.FixedSingle;

            panel.Controls.Add(title);
            Label stock = new Label();
            stock.Text = "Stock";
            stock.Font = new Font(stock.Font.FontFamily, 12, stock.Font.Style | FontStyle.Bold | FontStyle.Underline);
            stock.Location = new Point(15, 35);
            stock.AutoSize = true;
            panel.Controls.Add(stock);

            Label current = new Label();
            current.Text = "Current";
            current.Font = new Font(current.Font.FontFamily, 12, current.Font.Style | FontStyle.Bold | FontStyle.Underline);
            current.Location = new Point(75, 35);
            current.AutoSize = true;
            panel.Controls.Add(current);

            Label bid = new Label();
            bid.Text = "Bid";
            bid.Font = new Font(bid.Font.FontFamily, 12, bid.Font.Style | FontStyle.Bold | FontStyle.Underline);
            bid.Location = new Point(145, 35);
            bid.AutoSize = true;
            panel.Controls.Add(bid);

            Label askPrice = new Label();
            askPrice.Text = "Ask $";
            askPrice.Font = new Font(askPrice.Font.FontFamily, 12, askPrice.Font.Style | FontStyle.Bold | FontStyle.Underline);
            askPrice.Location = new Point(180, 35);
            askPrice.AutoSize = true;
            panel.Controls.Add(askPrice);

            addStocks(portfolio, list);

        }

        override
        public void refresh(StockPortfolio portfolio, ListBox list)
        {
            for (int i = 0; i < portfolio.Count; i++)
            {
                bool done = false;
                while (done == false)
                {

                    if (directions[i].InvokeRequired)
                    {
                        directions[i].Invoke(new MethodInvoker(delegate
                        {
                            if (portfolio[list.Items[i].ToString()].currentPrice >= lastPrices[i])
                            {
                                directions[i].Text = "^";
                                directions[i].ForeColor = Color.Green;
                            }
                            if (portfolio[list.Items[i].ToString()].currentPrice < lastPrices[i])
                            {
                                directions[i].Text = "v";
                                directions[i].ForeColor = Color.Red;
                            }
                        }));
                    }

                    if (currentPrices[i].InvokeRequired)
                    {
                        currentPrices[i].Invoke(new MethodInvoker(delegate
                        {
                            currentPrices[i].Text = portfolio[list.Items[i].ToString()].currentPrice.ToString();
                        }));
                    }

                    lastPrices[i] = portfolio[list.Items[i].ToString()].currentPrice;

                    if (bids[i].InvokeRequired)
                    {
                        bids[i].Invoke(new MethodInvoker(delegate
                        {
                            bids[i].Text = portfolio[list.Items[i].ToString()].bidPrice.ToString();
                        }));
                    }

                    if (askPrice[i].InvokeRequired)
                    {
                        askPrice[i].Invoke(new MethodInvoker(delegate
                        {
                            askPrice[i].Text = portfolio[list.Items[i].ToString()].askPrice.ToString();
                        }));
                    }

                    done = true;
                }
            }

        }

        override
        public void clear()
        {

        }

        public void addStocks(StockPortfolio portfolio, ListBox list)
        {
            for (int i = 0; i < portfolio.Count; i++)
            {
                Label stocks = new Label();
                stocks.Text = portfolio[list.Items[i].ToString()].symbol;
                stocks.Location = new Point(25, (i * 25) + 75);
                stocks.AutoSize = true;
                panel.Controls.Add(stocks);

                currentPrices.Add(new Label());
                currentPrices[i].Text = portfolio[list.Items[i].ToString()].currentPrice.ToString();
                currentPrices[i].Location = new Point(80, (i * 25) + 75);
                currentPrices[i].AutoSize = true;
                panel.Controls.Add(currentPrices[i]);

                directions.Add(new Label());
                lastPrices.Add(portfolio[list.Items[i].ToString()].currentPrice);
                directions[i].Text = "-";
                directions[i].Location = new Point(120, (i * 25) + 75);
                directions[i].AutoSize = true;
                panel.Controls.Add(directions[i]);

                bids.Add(new Label());
                bids[i].Text = portfolio[list.Items[i].ToString()].bidPrice.ToString();
                bids[i].Location = new Point(150, (i * 25) + 75);
                bids[i].AutoSize = true;
                panel.Controls.Add(bids[i]);

                askPrice.Add(new Label());
                askPrice[i].Text = portfolio[list.Items[i].ToString()].askPrice.ToString();
                askPrice[i].Location = new Point(185, (i * 25) + 75);
                askPrice[i].AutoSize = true;
                panel.Controls.Add(askPrice[i]);

            }
        }
    }
}
