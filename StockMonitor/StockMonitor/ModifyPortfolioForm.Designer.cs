﻿namespace StockMonitor
{
    partial class ModifyPortfolioForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBox_Market = new System.Windows.Forms.ListBox();
            this.listBox_Portfolio = new System.Windows.Forms.ListBox();
            this.button_Remove = new System.Windows.Forms.Button();
            this.button_Add = new System.Windows.Forms.Button();
            this.label_Market = new System.Windows.Forms.Label();
            this.label_Portfolio = new System.Windows.Forms.Label();
            this.button_Save = new System.Windows.Forms.Button();
            this.button_Cancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // listBox_Market
            // 
            this.listBox_Market.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listBox_Market.FormattingEnabled = true;
            this.listBox_Market.Location = new System.Drawing.Point(13, 39);
            this.listBox_Market.Name = "listBox_Market";
            this.listBox_Market.Size = new System.Drawing.Size(120, 368);
            this.listBox_Market.Sorted = true;
            this.listBox_Market.TabIndex = 0;
            // 
            // listBox_Portfolio
            // 
            this.listBox_Portfolio.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listBox_Portfolio.FormattingEnabled = true;
            this.listBox_Portfolio.Location = new System.Drawing.Point(220, 39);
            this.listBox_Portfolio.Name = "listBox_Portfolio";
            this.listBox_Portfolio.Size = new System.Drawing.Size(120, 368);
            this.listBox_Portfolio.Sorted = true;
            this.listBox_Portfolio.TabIndex = 1;
            // 
            // button_Remove
            // 
            this.button_Remove.Location = new System.Drawing.Point(139, 234);
            this.button_Remove.Name = "button_Remove";
            this.button_Remove.Size = new System.Drawing.Size(75, 23);
            this.button_Remove.TabIndex = 2;
            this.button_Remove.Text = "<------";
            this.button_Remove.UseVisualStyleBackColor = true;
            this.button_Remove.Click += new System.EventHandler(this.button_Remove_Click);
            // 
            // button_Add
            // 
            this.button_Add.Location = new System.Drawing.Point(139, 169);
            this.button_Add.Name = "button_Add";
            this.button_Add.Size = new System.Drawing.Size(75, 23);
            this.button_Add.TabIndex = 3;
            this.button_Add.Text = "------>";
            this.button_Add.UseVisualStyleBackColor = true;
            this.button_Add.Click += new System.EventHandler(this.button_Add_Click);
            // 
            // label_Market
            // 
            this.label_Market.AutoSize = true;
            this.label_Market.Location = new System.Drawing.Point(13, 20);
            this.label_Market.Name = "label_Market";
            this.label_Market.Size = new System.Drawing.Size(43, 13);
            this.label_Market.TabIndex = 4;
            this.label_Market.Text = "Market:";
            // 
            // label_Portfolio
            // 
            this.label_Portfolio.AutoSize = true;
            this.label_Portfolio.Location = new System.Drawing.Point(220, 20);
            this.label_Portfolio.Name = "label_Portfolio";
            this.label_Portfolio.Size = new System.Drawing.Size(48, 13);
            this.label_Portfolio.TabIndex = 5;
            this.label_Portfolio.Text = "Portfolio:";
            // 
            // button_Save
            // 
            this.button_Save.Location = new System.Drawing.Point(184, 426);
            this.button_Save.Name = "button_Save";
            this.button_Save.Size = new System.Drawing.Size(75, 23);
            this.button_Save.TabIndex = 14;
            this.button_Save.Text = "Save";
            this.button_Save.UseVisualStyleBackColor = true;
            this.button_Save.Click += new System.EventHandler(this.button_Save_Click);
            // 
            // button_Cancel
            // 
            this.button_Cancel.Location = new System.Drawing.Point(265, 426);
            this.button_Cancel.Name = "button_Cancel";
            this.button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.button_Cancel.TabIndex = 13;
            this.button_Cancel.Text = "Cancel";
            this.button_Cancel.UseVisualStyleBackColor = true;
            this.button_Cancel.Click += new System.EventHandler(this.button_Cancel_Click);
            // 
            // ModifyPortfolioForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(352, 461);
            this.Controls.Add(this.button_Save);
            this.Controls.Add(this.button_Cancel);
            this.Controls.Add(this.label_Portfolio);
            this.Controls.Add(this.label_Market);
            this.Controls.Add(this.button_Add);
            this.Controls.Add(this.button_Remove);
            this.Controls.Add(this.listBox_Portfolio);
            this.Controls.Add(this.listBox_Market);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "ModifyPortfolioForm";
            this.Text = "ModifyPortfolioForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBox_Market;
        private System.Windows.Forms.ListBox listBox_Portfolio;
        private System.Windows.Forms.Button button_Remove;
        private System.Windows.Forms.Button button_Add;
        private System.Windows.Forms.Label label_Market;
        private System.Windows.Forms.Label label_Portfolio;
        private System.Windows.Forms.Button button_Save;
        private System.Windows.Forms.Button button_Cancel;
    }
}